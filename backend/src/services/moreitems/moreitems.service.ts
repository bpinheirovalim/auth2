// Initializes the `moreitems` service on path `/moreitems`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Moreitems } from './moreitems.class';
import createModel from '../../models/moreitems.model';
import hooks from './moreitems.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'moreitems': Moreitems & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/moreitems', new Moreitems(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('moreitems');

  service.hooks(hooks);
}
